from unittest import TestCase
from pmanager import pmanager
import base64

class TestPmanager(TestCase):

    pm=pmanager()

    def test_get_master_password(self):
        self.assertEqual(base64.b64encode(self.pm.get_master_password(salt=b"b",password="a")),b"+44g/C5MPySMYMOb1lLzwTRymLuXe4tNWQO4UFViBgM=")

    def test_get_salt(self):
        self.assertEqual(len(base64.b64decode(self.pm.get_salt(12))), 12)

    def test_pad_text(self):
        self.assertEqual(len(self.pm.pad_text(b"s")),32)

    def test_unpad_text(self):
        test_string=self.pm.pad_text(b"a")
        self.assertEqual((self.pm.unpad_text(test_string)),b"a")

