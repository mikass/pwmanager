from pmanager import pmanager
import sqlite3
import sys
from getpass import getpass

class main:
    conn = sqlite3.connect('my_secret_database')
    c = conn.cursor()
    c.execute('''CREATE TABLE IF NOT EXISTS master (salt BLOB, password BLOB)''')
    c.execute('''CREATE TABLE IF NOT EXISTS sites (site TEXT, password BLOB)''')
    pm = pmanager()

    def search_account(self, masterkey):
        site = input("Enter servername for account you are looking for: ")
        search_site = self.c.execute("""SELECT site,password FROM sites WHERE site like ?""", ('%' + site + '%',))
        if search_site:
            for site in search_site:
                sitename = site[0]
                password = site[1]
                print("\n Sitename: " + sitename)
                print(" Password: " + self.pm.decrypt(password, masterkey))
                print(" ---------\n")
        else:
            print("No accounts found\n")

    def add_account(self, masterkey):
        site = input("Enter servername for account you want to add: ")
        password = getpass("Enter password for new account: ").encode()

        crypt_pass = self.pm.encrypt(password, masterkey)
        self.c.execute("""INSERT INTO sites (site,password) VALUES (?,?)""", (site, crypt_pass))
        self.conn.commit()
        print("Account added\n")

    def start(self):

        test_master = self.c.execute("SELECT * FROM master").fetchall()

        if test_master:
            master_pass = getpass("Enter your master password: ")
            get_master = self.c.execute("SELECT salt,password FROM master").fetchall()
            for meh in get_master:
                sa = meh[0]
                ha = meh[1]
            ma = self.pm.create_master_password(master_pass, sa)
            if (ma != ha):
                print("Incorrect password")
                sys.exit(1)
            masterkey = self.pm.get_master_password(master_pass, sa)
            while True:
                print(" 1 Add account\n"
                      " 2 Search for existing account\n"
                      " 3 Exit\n")
                choice = input(": ")

                if choice == "1":
                    self.add_account(masterkey)

                elif choice == "2":
                    self.search_account(masterkey)

                else:
                    sys.exit(1)
        else:
            master_pass = getpass("Running for first time, please enter yout master password: ")
            salt = self.pm.get_salt(12)
            masterkey = self.pm.create_master_password(master_pass, salt)

            self.c.execute("""INSERT INTO master (salt,password) VALUES (?,?)""", (salt, masterkey))
            self.conn.commit()

if __name__ == '__main__':
    boomheadshot = main()
    while True:
        boomheadshot.start()

