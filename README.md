# Simple Password manager in python

Just a simple project for Python coding class.

## Installation

```shell
git clone https://bitbucket.org/mikass/pwmanager
pip3 install -r requirements.txt
python password_manager.py
```
