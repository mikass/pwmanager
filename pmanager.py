import hashlib
import string
import random
from Crypto import Random
from Crypto.Cipher import AES
import base64


class pmanager(object):
    def __init__(self):
        self.chunksize = 32

    def create_master_password(self, password, salt):
        masterkey = hashlib.sha256(password.encode('utf8') + salt).digest()
        # hashing again to conceal true key
        hashed_masterkey = hashlib.sha256(masterkey + salt).digest()
        return base64.b64encode(hashed_masterkey)

    def get_master_password(self, password, salt):
        masterkey = hashlib.sha256(password.encode('utf8') + salt).digest()
        return masterkey

    def get_salt(self, lenght):
        salt = ''.join(random.choice(string.ascii_letters + string.digits + string.punctuation) for x in range(lenght))
        return base64.b64encode(salt.encode('utf-8'))

    def pad_text(self, s):
        leftover = len(s) % self.chunksize
        padstring = s + (b'\xaf' * (self.chunksize - leftover))
        return padstring

    def unpad_text(self, string):
        return string.replace(b'\xaf',b'')

    def encrypt(self, string, masterkey):
        raw = self.pad_text(string)
        iv = Random.new().read(AES.block_size)
        cipher = AES.new(masterkey, AES.MODE_EAX, iv)
        return base64.b64encode(iv + cipher.encrypt(raw)).decode('utf-8')

    def decrypt(self, cipherstring, masterkey):
        encode = base64.b64decode(cipherstring)
        iv = encode[:AES.block_size]
        cipher = AES.new(masterkey, AES.MODE_EAX, iv)
        return self.unpad_text(cipher.decrypt(encode[AES.block_size:])).decode('utf-8')